#include<stdio.h>
#include<math.h>
float get_radius()
{
  float radius;
  printf("enter radius\n");
  scanf("%f",&radius);
  return radius;
}
float compute_area(float radius)
{
  return M_PI*radius*radius;
}
float compute_circumference(float radius) 
{  
  return 2*M_PI*radius;
}
float output(float radius,float area,float circumference)
{
  printf("the area and circumference of the circle with radius=%f is area=%f circumference=%f\n",radius,area,circumference);
}
int main()
{
  float radius,area,circumference;
  radius = get_radius();
  area = compute_area(radius);
  circumference = compute_circumference(radius);
  output(radius,area,circumference);
  return 0;
}